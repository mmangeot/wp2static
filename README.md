# wp2static

Scripts pour déployer un site statique à partir d'un site wordpress

Pour installer le module wp2static, suivre les instructions indiquées dans install.sh
Attention, malgré son nom, ce script n'est pas exécutable.

Pour déployer automatiquement, adapter le script deploy.sh puis ajouter le script deployment_cron.pl dans votre crontab


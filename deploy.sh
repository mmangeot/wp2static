#!/bin/sh
# usage ./deploy.sh nomsite idsite
#site=archipel
site=$1
siteid=$2
cd /var/www/html/wordpress
/usr/local/bin/wp --allow-root --url=http://make-wp.inrialpes.fr/wordpress/$site/ wp2static full_workflow
cd wp-content/uploads/sites/$siteid/wp2static-processed-site
find . -type d -exec curl -s --netrc-file /root/.netrc -X MKCOL "https://files.inria.fr:8443/$site/{}" \;
find . -type f -exec curl -s --netrc-file /root/.netrc -T {} "https://files.inria.fr:8443/$site/{}" \;
